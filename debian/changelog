python-phply (1.2.6-1) unstable; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.
  * New upstream version (Closes: #1018552)
  * Bump copyright years
  * Bump watch file to v4
  * Add R^3: no

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 10 Jan 2023 14:46:19 +0100

python-phply (1.2.5-4) unstable; urgency=medium

  * Drop python2 package, for real this time
    (Closes: #938022)
  * Bump std-version to 4.4.1

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sun, 03 Nov 2019 12:38:45 +0100

python-phply (1.2.5-3) unstable; urgency=medium

  * Revert "Drop Python2 package (Closes: #938022)"
    - translate-toolkit/virtaal seems to be still far from going Python3

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 19 Sep 2019 09:10:00 +0200

python-phply (1.2.5-2) unstable; urgency=medium

  * Bump compat level to 12
  * Bump std-version to 4.4.0
  * Drop Python2 package (Closes: #938022)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 04 Sep 2019 13:32:52 +0200

python-phply (1.2.5-1) unstable; urgency=medium

  * New upstream version 1.2.5
  * Bump std-version to 4.2.0.1, no changes required
  * Switch VCS fields to salsa.d.o
  * Switch copyright in secure mode
  * Bump copyright years

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 06 Aug 2018 09:59:47 +0200

python-phply (1.2.4-1) unstable; urgency=medium

  [ Stuart Prescott ]
  *  New upstream version 1.2.4 (Closes: #893714)
    thanks Stuart Prescott for the report!
  * Fix conflicts between python-phply and python3-phply (Closes: #893708).

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 21 Mar 2018 15:55:55 +0100

python-phply (1.2.2-1) unstable; urgency=medium

  * New upstream version 1.2.2
  * Make watch file scan with secure url
  * Bump std-version to 4.1.3

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 18 Jan 2018 10:57:35 +0100

python-phply (1.2.1-1) unstable; urgency=medium

  * New upstream version 1.2.1

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 10 Nov 2017 11:41:31 +0100

python-phply (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0
  * Bump std-version to 4.1.1

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 26 Oct 2017 08:53:11 +0200

python-phply (1.0.0-1) unstable; urgency=medium

  * Bump std-version to 3.9.8
  * Fix insecure VCS fields.
  * New upstream release
  * Change homepage to new project location
  * Update copyright years
  * Drop patches, testsuite fixed upstream
  * Drop README file, removed upstream.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 16 May 2016 11:00:57 +0200

python-phply (0.9.1-4) unstable; urgency=medium

  * Change Python*:Depends to python:Depends.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 28 Sep 2015 09:02:30 +0200

python-phply (0.9.1-3) unstable; urgency=medium

  * tweak fix-tests.patch to fix another python-3 issue.
    (Closes: #796519) thanks Chris Lamb for the useful bug
    report!
  * Update my uid.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 26 Aug 2015 11:13:40 +0200

python-phply (0.9.1-2) unstable; urgency=low

  * python3-phply.pyremove, exclude tests from install. (Closes: #792766)
    thanks Andreas Beckmann for the bug report.

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Sat, 18 Jul 2015 12:46:59 +0200

python-phply (0.9.1-1) unstable; urgency=low

  [ Gianfranco Costamagna ]
  * New upstream release (actually this is just a tarball rename).
  * Move myself as maintainer and collab-maint as VCS.
    - DPMT rejected my team application.

  [ Piotr Ożarowski ]
  * Add watch file.
  * python-phply.pyremove, exclude tests from install. (Closes: #788838)
    - An equivalent fix will come directly in dh_python.

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Mon, 15 Jun 2015 08:43:10 +0200

python-phply (0.9.1~alpha-1) unstable; urgency=low

  * Initial release (Closes: #719811).

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Mon, 16 Feb 2015 18:42:22 +0100
